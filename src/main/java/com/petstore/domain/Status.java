package com.petstore.domain;


/**
 * Created by Dar.
 */

//Creation of Status Object

public enum Status {

    available("available"),
    pending("pending"),
    sold("sold");

    private String value;

    Status(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
