package com.petstore;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.Instant;

/**
 * Created by Dar.
 */

//Creation of random data for the the api requests
public class RandomData {

	public static String getStoreId(){
		String  generatedInt = RandomStringUtils.randomNumeric(1);
		return generatedInt;
	}
	
	public static String getStorePetId(){
		String  generatedInt = RandomStringUtils.randomNumeric(4);
		return generatedInt;
	}
	public static String getStoreQty(){
		String  generatedInt = RandomStringUtils.randomNumeric(4);
		return generatedInt;
	}
	
	public static String getPresentDate(){
		String  preDateTimeZ = Instant.now().toString();
		return preDateTimeZ;
	}
	
	public static String getStatusType(){
		String  statusType = "placed";
		return statusType;
	}
	
	public static String getCompleteStatus(){
		String  completeType = "true";
		return completeType;
	}
	
	public static int getOrderId(){
		int  generatedOrdId = 7;
		return generatedOrdId;
	}
	
	
	public static String getUserId(){
		String  generatedInt = RandomStringUtils.randomNumeric(1);
		return generatedInt;
	}
	
	public static String getUserName(){
		String  generatedInt = RandomStringUtils.randomNumeric(3);
		return ("Nick" + generatedInt);
	}
	
	public static String getFirstName(){
		String  generatedInt = RandomStringUtils.randomNumeric(3);
		return ("John" + generatedInt);
	}
	
	public static String getLastName(){
		String  generatedInt = RandomStringUtils.randomNumeric(3);
		return ("Wick" + generatedInt);
	}
	
	public static String getemail(){
		String  generatedInt = RandomStringUtils.randomNumeric(3);
		return ("JohnWick" + generatedInt + "@something.com");
	}
	
	public static String getPassword(){
		String  generatedInt = RandomStringUtils.randomNumeric(3);
		return ("pass" + generatedInt + "Nu12");
	}
	
	public static String getPhone(){
		String  generatedInt = RandomStringUtils.randomNumeric(10);
		return (generatedInt);
	}
	
	public static String getUserStatus(){
		String  generatedInt = RandomStringUtils.randomNumeric(1);
		return (generatedInt);
	}
}
