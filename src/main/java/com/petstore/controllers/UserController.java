package com.petstore.controllers;

import java.util.HashMap;
import com.petstore.RandomData;
import static io.restassured.RestAssured.given;
import static com.petstore.Constants.*;

/**
 * Created by Dar.
 */

//Controller to test the User apis in the petstore
public class UserController {
	public static HashMap Usermap = new HashMap();
	public static HashMap Updateusermap = new HashMap();
	public static HashMap Arraysermap = new HashMap();
	public static String USER_ENDPOINT = BASE_URL + "/user";
	
	//Create a new petstore user by Object body
	static public void createUser(String username) {
		Usermap.put("id", RandomData.getUserId());
		Usermap.put("username", username);
		Usermap.put("firstName", RandomData.getFirstName());
		Usermap.put("lastName", RandomData.getLastName());
		Usermap.put("email", RandomData.getemail());
		Usermap.put("password", RandomData.getPassword());
		Usermap.put("phone", RandomData.getPhone());
		Usermap.put("userStatus", RandomData.getUserStatus());
        given()
        .contentType("application/json")
        .body(Usermap)
        .post(USER_ENDPOINT)
        .then().statusCode(200)
        .log().all();
                
    }
	
	//fetch details of existing user by username
	static public void getUserByName(String username){
		given()
        .contentType("application/json")
        .get(USER_ENDPOINT + "/" + username)
        .then().statusCode(200)
        .log().all();
	}
	
	//check login for the user
	static public void loginUser(String username){
		given()
        .contentType("application/json")
        .get(USER_ENDPOINT + "/login")
        .then().statusCode(200)
        .log().all();
	}
	
	//Update details of an existing user by username
	static public void updateUser(String username) {
		Updateusermap.put("id", RandomData.getUserId());
		Updateusermap.put("username", username);
		Updateusermap.put("firstName", RandomData.getFirstName());
		Updateusermap.put("lastName", RandomData.getLastName());
		Updateusermap.put("email", RandomData.getemail());
		Updateusermap.put("password", RandomData.getPassword());
		Updateusermap.put("phone", RandomData.getPhone());
		Updateusermap.put("userStatus", RandomData.getUserStatus());
        given()
        .contentType("application/json")
        .body(Updateusermap)
        .put(USER_ENDPOINT + "/" + username)
        .then().statusCode(200)
        .log().all();
                
    }
	
	//delete an existing user by username
	static public void deleteUser(String username){
		given()
        .contentType("application/json")
        .delete(USER_ENDPOINT + "/" + username)
        .then().statusCode(200)
        .log().all();
	}
	
	//check logout for a logged in user
	static public void logoutUser(String username){
		given()
        .contentType("application/json")
        .get(USER_ENDPOINT + "/logout")
        .then().statusCode(200)
        .log().all();
	}
	
	
}
