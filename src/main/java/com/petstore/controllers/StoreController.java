package com.petstore.controllers;

import java.util.HashMap;
import com.petstore.RandomData;
import static io.restassured.RestAssured.given;
import static com.petstore.Constants.*;

/**
 * Created by Dar.
 */

//Controller to test the store apis in the petstore
public class StoreController {
	public static HashMap map = new HashMap();
	static String STORE_ENDPOINT = BASE_URL + "/store";
    
	//place an order in the store
	static public void placeOrder(int orderId) {
		map.put("id", orderId);
        map.put("petId", RandomData.getStorePetId());
        map.put("quantity", RandomData.getStoreQty());
        map.put("shipDate", RandomData.getPresentDate());
        map.put("status", RandomData.getStatusType());
        map.put("complete", RandomData.getCompleteStatus());
        given()
        .contentType("application/json")
        .body(map)
        .post(STORE_ENDPOINT + "/order")
        .then().statusCode(200)
        .log().all();
                
    }
	
	//fetch an existing order fro the store
	static public void fetchOrder(int orderId) {
		given()
        .contentType("application/json")
        .get(STORE_ENDPOINT + "/order/" + orderId)
        .then().statusCode(200)
        .log().all();
                
    }
	
	//delete an existing order fro the store
	static public void deleteOrder(int orderId) {
		given()
        .contentType("application/json")
        .get(STORE_ENDPOINT + "/order/" + orderId)
        .then().statusCode(200)
        .log().all();
                
    }
	
	//fetch the inventory for the store
	static public void fetchInventory() {
		given()
        .contentType("application/json")
        .get(STORE_ENDPOINT + "/inventory")
        .then().statusCode(200)
        .log().all();
                
    }

}
