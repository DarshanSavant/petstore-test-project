package com.petstore.controllers;

import com.petstore.domain.Pet;
import com.petstore.domain.Status;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import java.util.List;

import static io.restassured.RestAssured.given;
import static com.petstore.Constants.*;
import static org.hamcrest.CoreMatchers.containsString;

/**
 * Created by Dar.
 */

//Controller class to test the pet apis in the petstore

public class PetsController {
    public static String PET_ENDPOINT = BASE_URL + "/pet";
    private RequestSpecification requestSpecification;

    public PetsController() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri(BASE_URL);
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();
    }

    //addition of new pet to petstore
    public Pet addNewPet(Pet pet) {
        return given(requestSpecification)
                .body(pet)
                .post(PET_ENDPOINT)
                .as(Pet.class);
    }

    //get pets by their status
    public List<Pet> getPetsByStatus(Status status) {
        return given(requestSpecification)
                .queryParam("status", Status.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then()
                .statusCode(200)
                .extract().body()
                .jsonPath().getList("", Pet.class);

    }

    //delete an existing pet
    public void deletePet(Pet pet) {
        given(requestSpecification)
                .pathParam("petId", pet.getId())
                .delete(PET_ENDPOINT + "/{petId}")
                .then()
                .log().all()
                .extract().body();
                
    }

    //verification is a pet is deleted succssfully
    public void verifyPetDeleted(Pet pet) {
         given(requestSpecification)
                .pathParam("petId", pet.getId())
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .log().all()
                .body(containsString("Pet not found"));
    }

    //find an existing pet
    public Pet findPet(Pet pet) {
        return given(requestSpecification)
                .pathParam("petId", pet.getId())
                .get(PET_ENDPOINT + "/{petId}").as(Pet.class);
    }

    //Update the details of an existing pet
    public Pet updatePet(Pet pet) {
        return given(requestSpecification)
                .body(pet)
                .put(PET_ENDPOINT).as(Pet.class);
    }
    
    
    //Upload an image for an existing pet
    public void unsuppUploadPetImage(Pet pet) {
         		given(requestSpecification)
                .pathParam("petId", pet.getId())
                .post(PET_ENDPOINT + "/{petId}" + "/uploadImage")
                .then().statusCode(415);
                
                
    }
    
   

}
