Feature: petstore api testing

# Created by Dar
# Feature file to test the petstore apis

 Scenario: Add a new pet to the petstore and verify details
    Given the details of the pet are provided for postpet
    When the details of the pet are posted
    And the newly posted details are retrieved
    Then verify the response has the right details
 
 Scenario: find pets by status,delete details of a pet and verify deletion
    Given necessary pet detail is avaialble
    When list of pet is searched by status
    And an invalid id is tried to delete
    And a new id details are posted
    And a valid id is tried to delete
    Then verify the deletion of pet details
    
 Scenario: Update pet details and verify the details
    Given the details of the pet are provided
    And the name of the pet is updated
    Then verify the details changed
    
 Scenario: Verify the access to the Petstore orders
    Given an order is placed
    When details of order can be fetched
    And retrieve pet inventories by status
    Then delete the placed order

 Scenario: Test the operations about the user of petstore
    Given a new user is created of type object
    When the added user is verified by username
    And the user logs in
    And the details of user is updated
    And the user is then deleted
    Then the user logout is checked
 
