package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.ensure.Ensure;


import com.petstore.RandomData;
import com.petstore.controllers.PetsController;
import com.petstore.controllers.StoreController;
import com.petstore.controllers.UserController;
import com.petstore.domain.Category;
import com.petstore.domain.Pet;
import com.petstore.domain.Status;
import com.petstore.domain.Tag;
import com.petstore.Constants.*;
import org.apache.commons.lang3.RandomStringUtils;


import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

/**
 * Created by Dar.
 */

//Step definition class with implemention of the scenario steps

public class ApiStepDefinitions {
	
	private static final String PHOTO_URL = "https://www.tesco.ie/groceries/MarketingContent/Sites/Retail/superstore/Online/P/i/departments/2016/Pets/1BC.jpg";
	PetsController petsController;
    Pet pet = new Pet.Builder()
            .withId(RandomStringUtils.randomNumeric(10))
            .withName("My pet")
            .withPhotoUrls(Collections.singletonList(PHOTO_URL))
            .withStatus(Status.available)
            .withTags(Collections.singletonList(new Tag(1, "golden-retriever")))
            .inCategory(new Category(1, "dogs")).build();
    Pet petResponse;
    Status status;
    private static int orderId = RandomData.getOrderId();
    private static String username = RandomData.getUserName();
 
    
    //Add a new pet to the petstore and verify details
	@Given("the details of the pet are provided for postpet")
	public void the_details_of_the_pet_are_provided_for_postpet() {
        petsController = new PetsController();
    }
	
	@When("the details of the pet are posted")
	public void the_details_of_the_pet_are_posted() {
		petResponse = petsController.addNewPet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}		
	
	@And("the newly posted details are retrieved")
	public void the_newly_posted_details_are_retrieved() {
		petResponse = petsController.findPet(pet);
		
	}

	@Then("verify the response has the right details")
	public void verify_the_response_has_the_right_details() {
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}
	
	//Update pet details and verify the details
    
    @Given("the details of the pet are provided")
    public void the_details_of_the_pet_are_provided() {
    	petsController = new PetsController();
    }


    @When("the name of the pet is updated")
    public void the_name_of_the_pet_is_updated() {
    	
            pet.setName("Update pet name");
            Pet petResponse = petsController.updatePet(pet);
            assertThat(petResponse, is(samePropertyValuesAs(pet)));
    }

    @Then("verify the details changed")
    public void verify_the_details_changed() {
    	 Pet petResponse = petsController.findPet(pet);
         assertThat(petResponse, is(samePropertyValuesAs(pet)));
    }
	//find pets by status,delete details of a pet and verify deletion

    @Given("necessary pet detail is avaialble")
    public void necessary_pet_detail_is_avaialble() {
    	petsController = new PetsController();
    }

    @When("list of pet is searched by status")
    public void list_of_pet_is_searched_by_status() {
    	List<Pet> petResponseStatus = petsController.getPetsByStatus(status);
    }

    @And("an invalid id is tried to delete")
    public void also_if_details_of_a_pet_are_deleted() {
    	petsController.deletePet(pet);
    }
    
    @And("a new id details are posted")
    public void a_new_id_details_are_posted() {
    	petResponse = petsController.addNewPet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
    }
    
    @And("a valid id is tried to delete")
    public void a_valid_id_is_tried_to_delete() {
    	petsController.deletePet(pet);
    }
    
    @Then("verify the deletion of pet details")
    public void verify_the_deletion_of_pet_details() {
    	petsController.verifyPetDeleted(pet);
    }
    
    //Verify the access to the Petstore orders
    @Given("an order is placed")
    public void an_order_is_placed() {
    	StoreController.placeOrder(orderId);
    	
    }

    @When("details of order can be fetched")
    public void details_of_order_can_be_fetched() {
    	
    	StoreController.fetchOrder(orderId);
    }
    
    @And("retrieve pet inventories by status")
    public void retrieve_pet_inventories_by_status() {
    	
    	StoreController.fetchInventory();
    }

    @Then("delete the placed order")
    public void details_of_placed_order_is_verified() {
    	
    	StoreController.deleteOrder(orderId);
    }
    
    // Test the operations about the user of petstore
    @Given("a new user is created of type object")
    public void a_new_user_is_created_of_type_object() {
    	UserController.createUser(username);
    }

    @When("the added user is verified by username")
    public void the_added_user_is_verified_by_username() {
    	UserController.getUserByName(username);
    }

    @When("the user logs in")
    public void the_user_logs_in() {
        UserController.loginUser(username);
    }

    @When("the details of user is updated")
    public void the_details_of_user_is_updated() {
        UserController.updateUser(username);
    }

    @When("the user is then deleted")
    public void the_user_is_then_deleted() {
       UserController.deleteUser(username);
    }

    @Then("the user logout is checked")
    public void the_user_logout_is_cheked() {
        UserController.logoutUser(username);
    }
    
    

    
}
