# How to install, run and add new tests

# Petstore_Assignment
The project contains Gherkin test scenarios to test and verify all the apis in the petstore swagger io.


### Prerequisites

Maven must be setup on the local machine to run the test

### Installing

The following steps must be followed to install and run the tests locally

Navigate to the repository with link: https://gitlab.com/DarshanSavant/petstore-test-project.git
OR
please extract the java project added in the assignment and exteact the contents

And click on 'clone'

And choose to save the project as a zip file in any path on the local machine

Now navigate to the local path where the zip file is saved and unzip it

Open command prompt and follow instructions

cd "Directory name"
```
example : cd C:\Users\darshan\petstore-test
```  

After entering into the directory, provide the below command to trigger the test

```
mvn clean install
```  

### Add new tests
The project has a BDD structure so the test cases are created using the Gherkin.
Hence there are two major tests to follow to create a new test:

a. In the feature file "petstore_api_tests.feature"

src
  + main
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files

             petstore_api_tests.feature

,,,
in this file, create a new scenario like: 
Scenario: Update pet details and verify the details wiht the Given, When and then sections.
'''    
    
b. In the ApiStepDefinitions.java file, create the an implementation method for the scenarios defined in the feature file

src
  + main
  + test
    + java
      			ApiStepDefinitions.java
 	
### Test Results

After the tests are completed, the results of the locally run tests are stored in the 
\target\site\serenity
path of the project folder

